# README #

README.mdに画像を出すテスト

## テスト1
- ref
    - https://nkmrkisk.com/archives/1819

![Scheme](　${https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png}　)

## テスト2
- ref
    - https://qiita.com/rereton/items/b4488eb2176c9c4a5d7e
    
![代替テキスト](https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png, "画像タイトル")

## テスト3
 - ref
    - https://ja.confluence.atlassian.com/bitbucket/add-images-to-a-wiki-page-221451161.html
    
{{https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png}}

## テスト4
 - ref
     - https://hawk4-n.tumblr.com/post/78656850226/bitbucket%E3%81%AEmarkdown%E3%81%A7%E7%94%BB%E5%83%8F%E8%A1%A8%E7%A4%BA

![代替テキスト](https://bitbucket.org/tebine/readme-image/raw/f3764354e6109bdc122c462ac65874c1c8007de5/img/L.png)

https://bitbucket.org/tebine/readme-image/raw/f3764354e6109bdc122c462ac65874c1c8007de5/img/L.png

## テスト5 リサイズ
- ref
    - 話題には上がってるが、まだ機能はない？
    - https://gitlab.com/gitlab-org/gitlab-foss/issues/21189
    
![代替テキスト](https://bitbucket.org/tebine/readme-image/raw/f3764354e6109bdc122c462ac65874c1c8007de5/img/L.png)

